from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.db import IntegrityError

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from todos.models import TodoList, TodoItem

# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        import pprint

        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(context)
        return context


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]
    # success_url = reverse_lazy("todo_list_list")

    def get_success_url(self):
        return reverse("todo_list_detail", kwargs={"pk": self.object.pk})

    # def get_context_data(self, *args, **kwargs):
    #     context = super().get_context_data(*args, **kwargs)
    #     import pprint

    #     pp = pprint.PrettyPrinter(indent=4)
    #     pp.pprint(context)
    #     return context


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse("todo_list_detail", kwargs={"pk": self.object.pk})


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todo_items/new.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.pk])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todo_items/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
